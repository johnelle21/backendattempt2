var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message)
        throw err
    } else {
        console.log('Connected to the SQLite database.')
        db.run(`CREATE TABLE Hunters (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            lname text, 
            fname text, 
            email text UNIQUE, 
            contact INTEGER,
            password text, 
            CONSTRAINT email_unique UNIQUE (email)
            )`,
            (err) => {
                if (err) {
                    // Table already created
                } else {
                    // Table just created, creating some rows
                    console.log('About to insert ')
                    var insert = 'INSERT INTO Hunters (lname,fname, email, password) VALUES (?,?,?,?)'
                    db.run(insert, ["admin","admin", "admin@example.com", md5("admin123456")])
                    db.run(insert, ["user", "user", "user@example.com", md5("user123456")])
                    console.log('inserted ')
                }
            });
    }
});


module.exports = db
